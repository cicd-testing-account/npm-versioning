#!/usr/bin/env python3
import os
import re
import sys
import subprocess
import gitlab

def git(*args):
    return subprocess.check_output(["git"] + list(args))

def npm(*args):
    return subprocess.check_output(["npm"] + list(args))

def extract_gitlab_url_from_project_url():
    project_url = os.environ['CI_PROJECT_URL']
    project_path = os.environ['CI_PROJECT_PATH']
    return project_url.split(f"/{project_path}", 1)[0]

def extract_merge_request_id_from_commit():
    message = git("log", "-1", "--pretty=%B")
    matches = re.search(r'(\S*\/\S*!)(\d+)', message.decode("utf-8"), re.M|re.I)

    if matches == None:
        raise Exception(f"Unable to extract merge request from commit message: {message}")

    return matches.group(2)

def retrieve_labels_from_merge_request(merge_request_id):
    project_id = os.environ['CI_PROJECT_ID']
    gitlab_private_token = os.environ['NPA_PASSWORD']

    gl = gitlab.Gitlab(extract_gitlab_url_from_project_url(), private_token=gitlab_private_token)
    gl.auth()

    project = gl.projects.get(project_id)
    merge_request = project.mergerequests.get(merge_request_id)

    return merge_request.labels

def bump():
    merge_request_id = extract_merge_request_id_from_commit()
    labels = retrieve_labels_from_merge_request(merge_request_id)
    version = ""

    if "bump-minor" in labels:
        version = "minor"
    elif "bump-major" in labels:
        version = "major"
    else:
        version = "patch"

    npm('version', version)
    git('push', 'origin', 'main')

def main():
    bump()
    return 0

if __name__ == "__main__":
    sys.exit(main())
